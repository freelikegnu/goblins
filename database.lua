--- database functions consolidated here
-- create the tables if they do not exist!

goblins.db_fields = goblins.db:to_table()["fields"]

function goblins.db_deser(table)
  local data = minetest.deserialize(goblins.db_fields[table])
  return data
end

function goblins.db_read(table)
  local data = minetest.deserialize(goblins.db:to_table()["fields"][table])
  return data
end

function goblins.db_write(key, table)
  local data = minetest.serialize(table)
  goblins.db:set_string(key, data)
  return key, data
end

if not goblins.db_fields["territories"] then
    print("[goblins] -------------\n[goblins] must initialize territories!\n[goblins] -------------")
    goblins.db_write("territories", {
        test = {
            version = goblins.version,
            encode = minetest.encode_base64(os.date()),
            created = os.date()
        }
    })
end

if not goblins.db_fields["relations"] then
    print("[goblins] -------------\n[goblins] must initialize relations!\n[goblins] -------------")
    goblins.db_write("relations", {
        test = {
            version = goblins.version,
            encode = minetest.encode_base64(os.date()),
            created = os.date()
        }
    })
end

if not goblins.db_fields["territory_follow"] then
    print("[goblins] -------------\n[goblins] must initialize territory follow list!\n[goblins] -------------")
    goblins.db_write("territory_follow", {
        test = {
            version = goblins.version,
            encode = minetest.encode_base64(os.date()),
            created = os.date()
        }
    })
end

function goblins.db_update(fieldname,data,target_version,name)
  local fname = fieldname
  name = name or "unnammed"
  --[[
      print("[goblins] version: "..goblins.version..
      "\n[goblins] update target: "..target_version
  )
  if goblins.db_fields["content_version"] then print("[goblins] content version: "..
       tonumber( goblins.db_read("content_version")[1] ) ) end
  --]]
  if not goblins.db_fields[name .. "_version"] or
  tonumber( goblins.db_read(name .. "_version")[1] ) < tonumber( target_version ) then
      print("[goblins] Mod Storage entries for " ..fname.. " will be created or updated.")

      if goblins.db_fields[fieldname] then
          goblins.db_write(fname.."_backup_prior_to_"..target_version, goblins.db_read(fname))
          print("[goblins] Mod Storage entries for " ..fname.. " have been backed up")
      end

      goblins.db_write(fname, data)
      return true
  else
      print("[goblins] Mod Storage entries for " ..fname.. " are current")
      return false
  end
end

function goblins.db_fields_update(data,target_version,name)
  local dbs = data
  name = name or "unnamed"
  print("[goblins] version: ".. goblins.version.."\n[goblins] update target: "..target_version)
  if goblins.db_fields[name .. "_version"] then print("[goblins] " .. name .. "_version: ".. tonumber( goblins.db_read(name.. "_version")[1] ) ) end

  for k,v in pairs(dbs) do
      if not goblins.db_fields[k] then
          print("[goblins] creating non existant " .. name .. " key: " ..k
      --  .. " " ..dump ( dbs[k] )
       )
        goblins.db_write( k, dbs[k]
       )
      else
          print("[goblins] checking for updates to " .. name .. " key: " ..k )
          goblins.db_update( k, dbs[k], target_version, name )

          --print("[goblins] updating mod_content key: " ..k.. " " .. dump ( dbs[k] ) .."new data:")
          --print( dump ( goblins.db_read(k) ) )

      end
  end
end