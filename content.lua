-- ITEM DATABASE SETUP FOLLOWS - once the mod is run for the first time
-- server admins can then edit the worlds/<world>mod_storage.<type> database entries for the quest items
-- change or delete them in the DB at your pleasure
-- *** please do not edit this code, it will not overwrite the database unless there is a mod version update (backup entries will be created)***
-- best gifts to give goblins are first on the *follow* list and are of descending value thereafter 
-- the chance factor of receiving any drops is multiplied by the position that follow list (higher is worse chance)

local target_version = 20240824 --content entries earlier than this version will be backed up and overwritten with the below values

local db_content = {}

db_content = {
    copper_follow = {
        goblins.comp.default.gold_lump,
        goblins.comp.default.torch
        },
    copper_drops = {
        {name = goblins.comp.default.pick_diamond, chance = 1000, min = 0, max = 1},
        {name = goblins.comp.default.shovel_diamond, chance = 1000, min = 0, max = 1},
        {name = goblins.comp.default.axe_diamond, chance = 1000, min = 0, max = 1},
        {name = goblins.comp.default.pick_bronze, chance = 10, min = 0, max = 1},
        {name = goblins.comp.default.bronze_ingot, chance = 7, min = 0, max = 1},
        {name = goblins.comp.default.axe_bronze, chance = 5, min = 0, max = 1}
        },
    iron_follow = {
        goblins.comp.default.gold_lump,
        goblins.comp.default.torch
        },
    iron_drops = {
        {name = goblins.comp.default.pick_diamond, chance = 1000, min = 0, max = 1},
        {name = goblins.comp.default.shovel_diamond, chance = 1000, min = 0, max = 1},
        {name = goblins.comp.default.axe_diamond, chance = 1000, min = 0, max = 1},
        {name = goblins.comp.default.pick_steel, chance = 10, min = 0, max = 1},
        {name = goblins.comp.default.steel_ingot, chance = 7, min = 0, max = 1},
        {name = goblins.comp.default.axe_steel, chance = 5, min = 0, max = 1}
        },
    gold_follow = {
        goblins.comp.default.gold_lump,
        goblins.comp.default.torch
        },
    gold_drops = {
        {name = goblins.comp.default.pick_diamond, chance = 1000, min = 0, max = 1},
        {name = goblins.comp.default.shovel_diamond, chance = 1000, min = 0, max = 1},
        {name = goblins.comp.default.axe_diamond, chance = 1000, min = 0, max = 1},
        {name = goblins.comp.default.pick_gold, chance = 100, min = 0, max = 1},
        {name = goblins.comp.default.gold_lump, chance = 7, min = 0, max = 1},
        {name = goblins.comp.default.pick_bronze, chance = 5, min = 0, max = 1}
        },
    diamond_follow = {
        goblins.comp.default.diamond,
        goblins.comp.default.torch
        },
    diamond_drops = {
        {name = goblins.comp.default.pick_mese, chance = 1000, min = 0, max = 1},
        {name = goblins.comp.default.shovel_mese, chance = 1000, min = 0, max = 1},
        {name = goblins.comp.default.axe_mese, chance = 1000, min = 0, max = 1},
        {name = goblins.comp.default.pick_diamond, chance = 100, min = 0, max = 1},
        {name = goblins.comp.default.diamond, chance = 7, min = 0, max = 1},
        {name = goblins.comp.default.pick_bronze, chance = 5, min = 0, max = 1}
        },
    hoarder_follow = {
        goblins.comp.default.diamond,
        goblins.comp.default.torch
    },
    hoarder_drops = {
        {name = goblins.comp.default.mese_lamp, chance = 1000, min = 0, max = 1},
        {name = goblins.comp.default.pick_mese, chance = 1000, min = 0, max = 1},
        {name = goblins.comp.default.shovel_mese, chance = 10, min = 0, max = 1},
        {name = goblins.comp.default.mese_crystal, chance = 7, min = 0, max = 1},
        {name = goblins.comp.default.pick_bronze, chance = 5, min = 0, max = 1}
        },

    -- basic drops that all goblins will have in addition to tool drops
    gob_drops = {
        {name = goblins.comp.default.torch, chance = 4, min = 0, max = 10},
        {name = goblins.comp.default.flint, chance = 3, min = 0, max = 2},
        {name = goblins.comp.default.mossycobble, chance = 3, min = 0, max = 3},
        {
            name = "goblins:goblins_goblin_bone_meaty",
            chance = 3,
            min = 0,
            max = 1
        }, {name = "goblins:goblins_goblin_bone", chance = 2, min = 0, max = 3},
        {name = "goblins:mushroom_goblin", chance = 2, min = 0, max = 5}
        },

    -- best gifts are first on the follow list and are of descending value thereafter
    template_follow = {
        goblins.comp.default.mese, goblins.comp.default.diamond, goblins.comp.default.gold_lump,
        goblins.comp.default.torch,
        goblins.comp.default.stick,
    },

    -- these are tool drops if not already defined
    template_drops = {
        {name = goblins.comp.default.pick_steel, chance = 1000, min = 0, max = 1},
        {name = goblins.comp.default.shovel_steel, chance = 1000, min = 0, max = 1},
        {name = goblins.comp.default.axe_steel, chance = 1000, min = 0, max = 1},
        {name = "goblins:pick_mossycobble", chance = 10, min = 0, max = 1},
        {name = goblins.comp.default.axe_stone, chance = 5, min = 0, max = 1}
    },

    gobdog_follow = goblins.comp.default.gobdog_follow,

    gobdog_drops =
        {
            {name = "goblins:goblins_goblin_bone", chance = 1, min = 1, max = 3}
    },

    -- addtional follow foods defined in the compat files
    follow_foods = goblins.comp.default.follow_foods,

    goblin_chest_items = {
        {name = goblins.comp.default.torch, chance = 1, min = 5, max = 20},
        {
            name = "goblins:goblins_goblin_bone_meaty",
            chance = 1,
            min = 2,
            max = 10
        },
        {name = goblins.comp.mobs.shears, chance = 2, min = 1, max = 1},
        {name = goblins.comp.default.steel_ingot, chance = 2, min = 5, max = 10},
        {name = goblins.comp.default.gold_ingot, chance = 2, min = 5, max = 10},
        {name = goblins.comp.default.diamond, chance = 2, min = 3, max = 10},
        {name = goblins.comp.default.meselamp, chance = 2, min = 1, max = 10},
        {name = goblins.comp.default.shovel_diamond, chance = 5, min = 1, max = 1},
        {name = goblins.comp.default.axe_diamond, chance = 5, min = 1, max = 1},
        {name = goblins.comp.default.sword_diamond, chance = 5, min = 1, max = 1},
        {name = goblins.comp.default.shovel_mese, chance = 10, min = 1, max = 1},
        {name = goblins.comp.default.axe_mese, chance = 10, min = 1, max = 1},
        {name = goblins.comp.default.sword_mese, chance = 10, min = 1, max = 1}
    },
}

goblins.db_fields_update(db_content,target_version,"content")

goblins.db_write("content_version", {
    goblins.version
})

print("[goblins] content version: ".. goblins.db_read("content_version")[1])