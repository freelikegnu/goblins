-- this file sets content names for Minetest Game 
goblins.compat_mode = "redo"

--if   
     --hades_core and
     --hades_sounds and
     --hades_flowers and
     --hades_chests and
     --hades_farming then

    --print("all Hades mods found!")
--end

goblins.comp = {
    gobdef= {
        walk_velocity = 2,
        run_velocity = 3
    },
    default = {
        -- nodes
        mossycobble = "hades_core:mossycobble",
        slab_mossycobble = "hades_stairs:slab_mossycobble",
        dirt = "hades_core:dirt",
        gravel = "hades_core:gravel",
        cobble = "hades_core:cobble",
        deepslate = "hades_core:cobble",
        cactus = "hades_core:cactus",
        grass = {
           function(arg)
                arg = arg or 5    
                local grassn = "hades_grass:grass_" .. tostring(math.random(arg))
                return grassn
            end
        },
        lava_source = "hades_core:lava_source",
        obsidian = "hades_core:obsidian",
        diamond = "hades_core:diamond",
        mese = "hades_core:mese",
        desert_stone = "hades_core:desert_stone",
        stone = "hades_core:stone",
        stone_with_coal = "hades_core:stone_with_coal",
        stone_with_copper = "hades_core:stone_with_copper",
        stone_with_iron = "hades_core:stone_with_iron",
        stone_with_gold = "hades_core:stone_with_gold",
        stone_with_diamond = "hades_core:stone_with_diamond",
        chest = "hades_chests:chest",
        chest_locked = "hades_chests:chest_locked",
        --foods will be learned by the goblins observing the player
        follow_foods = {},
        gobdog_follow = {
            "goblins:goblins_goblin_bone",
            "goblins:goblins_goblin_bone_meaty",
        },
        -- tools
        axe_wood = "hades_core:axe_wood",
        axe_stone = "hades_core:axe_stone",
        axe_bronze = "hades_core:axe_bronze",
        axe_steel = "hades_core:axe_steel",
        axe_diamond = "hades_core:axe_prism",
        axe_mese = "hades_core:axe_mese",
        pick_wood = "hades_core:pick_wood",
        pick_stone = "hades_core:pick_stone",
        pick_bronze = "hades_core:pick_bronze",
        pick_steel = "hades_core:pick_steel",
        pick_gold = "hades_core:pick_gold",
        pick_diamond = "hades_core:pick_prism",
        pick_mese = "hades_core:pick_mese",
        shovel_wood = "hades_core:shovel_wood",
        shovel_stone = "hades_core:shovel_stone",
        shovel_bronze = "hades_core:shovel_bronze",
        shovel_steel = "hades_core:shovel_steel",
        shovel_diamond = "hades_core:shovel_prism",
        shovel_mese = "hades_core:shovel_mese",
        sword_wood = "hades_core:sword_wood",
        sword_stone = "hades_core:sword_stone",
        sword_bronze = "hades_core:sword_bronze",
        sword_steel = "hades_core:sword_steel",
        sword_gold = "hades_core:sword_steel",
        sword_diamond = "hades_core:sword_prism",
        sword_mese = "hades_core:sword_mese",
        -- items
        torch = "hades_torches:torch",
        stick = "hades_core:stick",
        flint = "hades_core:coal_lump",
        bronze_ingot = "hades_core:bronze_ingot",
        steel_ingot = "hades_core:steel_ingot",
        gold_ingot = "hades_core:gold_ingot",

        coal_lump = "hades_core:coal_lump",
        iron_lump = "hades_core:iron_lump",
        gold_lump = "hades_core:gold_lump",
        mese_lamp = "hades_core:mese_crystal",
        mese_crystal = "hades_core:mese_crystal"
    },
    fire = {
            basic_flame = "hades_core:lava_source"
    },
    danger_nodes = {"hades_core:lava_source","hades_core:lava_flowing","tnt:tnt"},
    liquid_nodes = {"hades_core:water_source","hades_core:water_flowing"},
    flowers = {
        mushroom_brown = "goblins:mushroom_goblin",
        mushroom_red = "goblins:mushroom_goblin"
    },

    mobs = {shears = "mobs:shears"},

    png = {
        cobble = "default_cobble.png",
        deepslate = "default_cobble.png",
        dirt = "default_dirt.png",
        stones = "default_stone.png",
        grass_1 = "mtg_grass_1.png",
        moss = "mtg_moss.png",
        lava = "default_lava.png",
        mossycobble = "default_mossycobble.png",
        tool_stonepick = "default_tool_stonepick.png",
        mineral_coal = "default_mineral_coal.png",
        mineral_copper = "default_mineral_copper.png",
        mineral_tin = "default_mineral_tin.png",
        mineral_iron = "default_mineral_iron.png",
        mineral_gold = "default_mineral_gold.png",
        mineral_diamond = "default_mineral_diamond.png"

    }
}

--work-around for my bad content entry
minetest.register_alias("hades_core:torch", "hades_torches:torch")

goblins.invis = mobs.invis

function goblins:register_egg(...)
    mobs:register_egg(...)
end

function goblins:register_mob(...)
    mobs:register_mob(...)
end

function goblins:spawn(def)
    mobs:spawn(def)
end

function goblins:set_animation(...)
    mobs:set_animation(...)
end

function goblins.node_sound_stone_defaults(...)
  --why is the hades_sounds global disappearing?!?!
  hades_sounds.node_sound_stone_defaults(...)
end

function goblins.node_sound_dirt_defaults(...)
  hades_sounds.node_sound_dirt_defaults(...)
end

function goblins.node_sound_leaves_defaults(...)
  hades_sounds.node_sound_grass_defaults(...)
end

function goblins.goblin_dog_rightclick(self, clicker)
    if mobs:feed_tame(self, clicker, 4, true, true) then return end
    if mobs:protect(self, clicker) then return end
    if mobs:capture_mob(self, clicker, 0, 5, 50, false, nil) then return end
end
