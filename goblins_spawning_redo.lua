-- ITEM DATABASE SETUP FOLLOWS - once the mod is run for the first time
-- server admins can then edit the worlds/<world>mod_storage.<type> database entries for the quest items
-- change or delete them in the DB at your pleasure
-- *** please do not edit this code, it will not overwrite the database ***

local target_version = 20230101

--print("***SPAWNING DB VERSION: " .. tonumber( goblins.db_read("spawning_version")[1]))

local db_spawning = {}

db_spawning = {

    digger_spawning = {
        nodes = {"group:stone","group:material_stone" },
        neighbors = "air",
        min_light = 0,
        max_light = 10,
        interval = 30,
        chance = 1000,
        active_object_count = 2,
        min_height = -31000,
        max_height = -15,
        day_toggle = nil,
        on_spawn = nil
    },

    cobble_spawning = {
        nodes = {"group:stone","group:material_stone"},
        neighbors = "air",
        min_light = 0,
        max_light = 10,
        interval = 30,
        chance = 1000,
        active_object_count = 2,
        min_height = -31000,
        max_height = -15,
        day_toggle = nil,
        on_spawn = nil
    },

    snuffer_spawning = {
        nodes = {goblins.comp.default.mossycobble},
        neighbors = "air",
        min_light = 0,
        max_light = 10,
        interval = 30,
        chance = 1000,
        active_object_count = 2,
        min_height = -31000,
        max_height = -15,
        day_toggle = nil,
        on_spawn = nil
    },

    fungiler_spawning = {
        nodes = {goblins.comp.default.mossycobble},
        neighbors = "air",
        min_light = 0,
        max_light = 10,
        interval = 30,
        chance = 1500,
        active_object_count = 1,
        min_height = -31000,
        max_height = -15,
        day_toggle = nil,
        on_spawn = nil
    },

    coal_spawning = {
        nodes = {goblins.comp.default.stone_with_coal, goblins.comp.default.mossycobble},
        neighbors = "air",
        min_light = 0,
        max_light = 10,
        interval = 20,
        chance = 500,
        active_object_count = 3,
        min_height = -31000,
        max_height = -25,
        day_toggle = nil,
        on_spawn = nil
    },

    copper_spawning = {
        nodes = {
            goblins.comp.default.stone_with_copper, goblins.comp.default.mossycobble,},
        neighbors = "air",
        min_light = 0,
        max_light = 10,
        interval = 30,
        chance = 500,
        active_object_count = 2,
        min_height = -31000,
        max_height = -35,
        day_toggle = nil,
        on_spawn = nil
    },

    iron_spawning = {
        nodes = {goblins.comp.default.stone_with_iron, goblins.comp.default.mossycobble},
        neighbors = "air",
        min_light = 0,
        max_light = 10,
        interval = 20,
        chance = 500,
        active_object_count = 3,
        min_height = -31000,
        max_height = -35,
        day_toggle = nil,
        on_spawn = nil
    },

    gold_spawning = {
        nodes = {goblins.comp.default.stone_with_gold, goblins.comp.default.mossycobble},
        neighbors = "air",
        min_light = 0,
        max_light = 10,
        interval = 30,
        chance = 500,
        active_object_count = 2,
        min_height = -31000,
        max_height = -100,
        day_toggle = nil,
        on_spawn = nil
    },

    diamond_spawning = {
        nodes = {goblins.comp.default.stone_with_diamond, goblins.comp.default.mossycobble},
        neighbors = "air",
        min_light = 0,
        max_light = 10,
        interval = 60,
        chance = 1000,
        active_object_count = 2,
        min_height = -31000,
        max_height = -200,
        day_toggle = nil,
        on_spawn = nil
    },

    hoarder_spawning = {
        nodes = {goblins.comp.default.mossycobble, goblins.comp.default.chest},
        neighbors = "air",
        min_light = 0,
        max_light = 10,
        interval = 90,
        chance = 2000,
        active_object_count = 1,
        min_height = -31000,
        max_height = -20,
        day_toggle = nil,
        on_spawn = nil
    },

    gobdog_spawning = {
        nodes = {goblins.comp.default.mossycobble, "group:sand", "group:dirt"},
        min_light = 0,
        max_light = 14,
        chance = 500,
        active_object_count = 4,
        min_height = -31000,
        max_height = -20,
        day_toggle = nil,
        on_spawn = nil
    },

    gobdog_aggro_spawning = {
        nodes = {goblins.comp.default.mossycobble, "group:sand", "group:dirt"},
        min_light = 0,
        max_light = 6,
        chance = 500,
        active_object_count = 1,
        min_height = -31000,
        max_height = -100,
        day_toggle = nil,
        on_spawn = nil
    },

    template_spawning = {
        nodes = {goblins.comp.default.mossycobble},
        min_light = 0,
        max_light = 10,
        chance = 500,
        active_object_count = 4,
        min_height = -31000,
        max_height = -20,
        day_toggle = nil,
        on_spawn = nil
    }

}

goblins.db_fields_update(db_spawning,target_version,"spawning")

goblins.db_write("spawning_version", {
    goblins.version
})

print("[goblins] spawning version: ".. goblins.db_read("spawning_version")[1])

--[[compatibility vars
local spawning_names = {
    "digger", "fungiler", "snuffer", "coal", "copper", "iron", "gold",
    "hoarder", "template", "gobdog", "gobdog_aggro"
}

goblins.spawning = {}
for _, name in ipairs(spawning_names) do
    goblins.spawning[name] = goblins.db_read(name .. "_spawning")
end
--print("spawning_db: "..dump(goblins.spawning))
]]
