-- this file sets content names for Minetest Game 
goblins.compat_mode = "redo"
goblins.comp = {
    gobdef= {
        walk_velocity = 2,
        run_velocity = 3
    },
    default = {
        -- nodes
        mossycobble = "goblins:cobble_with_moss", --no mossycobble :P
        slab_mossycobble = "partialblocks:slab_cobble",
        dirt = "rp_default:dirt",
        gravel = "rp_default:gravel",
        cobble = "rp_default:cobble",
        deepslate = "rp_default:cobble",
        cactus = "rp_default:cactus",
        grass = "rp_default:swamp_grass",
         
        lava_source = "rp_default:swamp_water_source",
        obsidian = "rp_lumien:crystal_off", --no obsidian
        diamond = "rp_lumien:crystal_off", --no diamond
        mese = "rp_default:mese",
        desert_stone = "rp_default:desert_stone",
        stone = "rp_default:stone",
        stone_with_coal = "rp_default:stone_with_coal",
        stone_with_copper = "rp_default:stone_with_copper",
        stone_with_iron = "rp_default:stone_with_iron",
        stone_with_gold = "rp_default:stone_with_gold",
        stone_with_diamond = "rp_lumien:stone_with_lumien",--no diamond
        chest = "rp_default:chest",
        chest_locked = "rp_default:chest_locked",
        --foods will be learned by the goblins observing the player
        follow_foods = {},
        gobdog_follow = {
            "goblins:goblins_goblin_bone",
            "goblins:goblins_goblin_bone_meaty",
        },
        -- tools
        axe_wood = "rp_default:axe_wood",
        axe_stone = "rp_default:axe_stone",
        axe_bronze = "rp_default:axe_bronze",
        axe_steel = "rp_default:axe_steel",
        axe_diamond = "rp_default:axe_bronze",--no diamond
        axe_mese = "rp_default:axe_mese",
        pick_wood = "rp_default:pick_wood",
        pick_stone = "rp_default:pick_stone",
        pick_bronze = "rp_default:pick_bronze",
        pick_steel = "rp_default:pick_steel",
        pick_gold = "rp_default:pick_carbon_steel",
        pick_diamond = "rp_default:pick_bronze", --no diamond
        pick_mese = "rp_default:pick_mese",
        shovel_wood = "rp_default:shovel_wood",
        shovel_stone = "rp_default:shovel_stone",
        shovel_bronze = "rp_default:shovel_bronze",
        shovel_steel = "rp_default:shovel_steel",
        shovel_diamond = "rp_default:shovel_bronze", --no diamond
        shovel_mese = "rp_default:shovel_mese",
        sword_wood = "rp_default:sword_wood",
        sword_stone = "rp_default:sword_stone",
        sword_bronze = "rp_default:sword_bronze",
        sword_steel = "rp_default:sword_steel",
        sword_gold = "rp_default:sword_carbon_steel",
        sword_diamond = "rp_default:sword_bronze", --no diamond
        sword_mese = "rp_default:sword_mese",
        -- items
        torch = "rp_default:torch",
        stick = "rp_default:stick",
        flint = "rp_default:flint",
        bronze_ingot = "rp_default:bronze_ingot",
        steel_ingot = "rp_default:steel_ingot",
        gold_ingot = "rp_default:gold_ingot",

        coal_lump = "rp_default:coal_lump",
        iron_lump = "rp_default:iron_lump",
        gold_lump = "rp_default:gold_lump",
        mese_lamp = "rp_lumien:block", --lunien
        mese_crystal = "rp_lumien:block"
    },
    fire = {
        basic_flame = "rp_fire:bonfire_burning"
    },
    danger_nodes = {
      --"rp_defaultlava_source","rp_defaultlava_flowing",
      "tnt:tnt"},
    liquid_nodes = {"rp_default:water_source","rp_default:water_flowing"},
    flowers = {
        mushroom_brown = "goblins:mushroom_goblin",
        mushroom_red = "goblins:mushroom_goblin"
    },

    mobs = {shears = "rp_default:shears"},

    png = {
        cobble = "default_cobbles.png",
        deepslate = "default_cobbles.png",
        dirt = "default_dirt.png",
        stones = "default_stone.png",
        grass_1 = "default_swamp_grass_clump.png",
        moss = "default_swamp_grass.png",
        lava = "default_swamp_water.png", --no lava in game
        mossycobble = "default_swamp_grass.png", --no mossycobble :P
        tool_stonepick = "default_pick_stone.png",
        mineral_coal = "default_mineral_coal.png",
        mineral_copper = "default_mineral_copper.png",
        mineral_tin = "default_mineral_tin.png",
        mineral_iron = "default_mineral_iron.png",
        mineral_gold = "gold_mineral_gold.png",
        mineral_diamond = "lumien_mineral_lumien.png"

    }
}

goblins.invis = mobs.invis

function goblins:register_egg(...)
    mobs:register_egg(...)
end

function goblins:register_mob(...)
    mobs:register_mob(...)
end

function goblins:spawn(def)
    mobs:spawn(def)
end

function goblins:set_animation(...)
    mobs:set_animation(...)
end

function goblins.node_sound_stone_defaults(...)
  rp_sounds.node_sound_stone_defaults(...)
end

function goblins.node_sound_dirt_defaults(...)
    rp_sounds.node_sound_dirt_defaults(...)
end

function goblins.node_sound_leaves_defaults(...)
  rp_sounds.node_sound_leaves_defaults(...)
end

function goblins.goblin_dog_rightclick(self, clicker)
    if mobs:feed_tame(self, clicker, 4, true, true) then return end
    if mobs:protect(self, clicker) then return end
    if mobs:capture_mob(self, clicker, 0, 5, 50, false, nil) then return end
end
