-- this file sets content names for MineClone2 Game 
goblins.compat_mode = "mc2"
goblins.comp = {
	  gobdef= {
							walk_velocity = 1,
							run_velocity = 2
		},
    default = {
        -- nodes
        mossycobble = "mcl_core:mossycobble",
        slab_mossycobble = "mcl_stairs:slab_mossycobble",
        dirt = "mcl_core:dirt",
        gravel = "mcl_core:gravel",
        cobble = "mcl_core:cobble",
        deepslate = "mcl_deepslate:deepslate",
        cactus = "mcl_core:cactus",
        grass = "mcl_flowers:tallgrass",
        lava_source = "mcl_core:lava_source",
        obsidian = "mcl_core:obsidian",
        diamond = "mcl_core:diamond",
        mese = "mcl_core:netherite",
        desert_stone = "mcl_core:desert_stone",
        stone = "mcl_core:stone",
        stone_with_coal = "mcl_core:stone_with_coal",
        stone_with_copper = "mcl_core:stone_with_copper",
        stone_with_iron = "mcl_core:stone_with_iron",
        stone_with_gold = "mcl_core:stone_with_gold",
        stone_with_diamond = "mcl_core:stone_with_diamond",
        chest = "mcl_chests:chest",
        chest_locked = "mcl_chests:chest",
				--foods will be learned by the goblins observing the player
        follow_foods = {},
				gobdog_follow = {
					"goblins:goblins_goblin_bone",
					"goblins:goblins_goblin_bone_meaty",
					"mcl_mobitems:beef",
					"mcl_mobitems:mutton",
					"mcl_mobitems:porkchop",
					"mcl_mobitems:chicken",
					"mcl_mobitems:bone",
				},
        -- tools
        axe_wood = "mcl_tools:axe_wood",
        axe_stone = "mcl_tools:axe_stone",
        axe_bronze = "mcl_tools:axe_iron",
        axe_steel = "mcl_tools:axe_iron",
        axe_gold = "mcl_tools:axe_gold",
        axe_diamond = "mcl_tools:axe_diamond",
        axe_mese = "mcl_tools:axe_netherite",
        pick_wood = "mcl_tools:pick_wood",
        pick_stone = "mcl_tools:pick_stone",
        pick_bronze = "mcl_tools:pick_iron",
        pick_steel = "mcl_tools:pick_iron",
        pick_gold = "mcl_tools:pick_gold",
        pick_diamond = "mcl_tools:pick_diamond",
        pick_mese = "mcl_tools:pick_netherite",
        shovel_wood = "mcl_tools:shovel_wood",
        shovel_stone = "mcl_tools:shovel_wood",
        shovel_bronze = "mcl_tools:shovel_iron",
        shovel_steel = "mcl_tools:shovel_iron",
        shovel_gold = "mcl_tools:shovel_gold",
        shovel_diamond = "mcl_tools:shovel_diamond",
        shovel_mese = "mcl_tools:shovel_netherite",
        sword_wood = "mcl_tools:sword_wood",
        sword_stone = "mcl_tools:sword_stone",
        sword_bronze = "mcl_tools:sword_iron",
        sword_steel = "mcl_tools:sword_iron",
        sword_gold = "mcl_tools:sword_gold",
        sword_diamond = "mcl_tools:sword_diamond",
        sword_mese = "mcl_tools:sword_netherite",
        -- items
        torch = "mcl_torches:torch",
        stick = "mcl_core:stick",
        flint = "mcl_core:flint",
        bronze_ingot = "mcl_copper:copper_ingot",
        steel_ingot = "mcl_core:iron_ingot",
        gold_ingot = "mcl_core:gold_ingot",

        coal_lump = "mcl_core:coal_lump",
        iron_lump = "mcl_core:iron_lump",
        gold_lump = "mcl_core:gold_lump",
        mese_lamp = "mesecons_torch:redstoneblock",
        mese_crystal = "mcl_end:crystal"
    },
    fire = {
      basic_flame = "mcl_fire:fire"
    },
		danger_nodes = {"mcl_core:lava_source","mcl_core:lava_flowing","mcl_tnt:tnt"},
		liquid_nodes = {"mcl_core:water_source","mcl_core:water_flowing"},
    flowers = {
        mushroom_brown = "mcl_mushrooms:mushroom_brown",
        mushroom_red = "mcl_mushrooms:mushroom_red"
    },

    mobs = {shears = "mcl_tools:shears"},

    png = {
        cobble = "default_cobble.png",
        deepslate = "mcl_deepslate.png",
        dirt = "default_dirt.png",
        stones = "default_stone.png",
        grass_1 = "mtg_grass_1.png",
        moss = "mtg_moss.png",
        lava = "mtg_lava.png",
        mossycobble = "default_mossycobble.png",
        tool_stonepick = "default_tool_stonepick.png",
        mineral_coal = "mcl_core_coal_ore.png",
        mineral_copper = "mcl_copper_ore.png",
        mineral_tin = "mcl_copper_ore.png",
        mineral_iron = "mcl_core_iron_ore.png",
        mineral_gold = "mcl_core_gold_ore.png",
        mineral_diamond = "mcl_core_diamond_ore.png"

    }
}

local cave_biomes = {}
--generates a list of all biomes that are underground in mc2 and mcla
minetest.register_on_mods_loaded(function()
	local mrb = minetest.registered_biomes
	for k,_ in pairs(mrb) do
	--print(k)
	 if string.find(k,"_underground") then
		table.insert(cave_biomes, k)
	 end
	end
	--print(dump(cave_biomes))
end)


local aoc_range = 10

goblins.invis = mcl_mobs.invis

function goblins:register_mob(...)
    mcl_mobs.register_mob(...)
end

function goblins:register_egg(...)
    mcl_mobs.register_egg(...)
end

function goblins:set_animation(...)
    mcl_mobs.set_animation(...)
end

if mcl_mobs and mcl_mobs.spawn_setup then
  print("spawn setup detected")
end

-- fill in any missing spawn definitions that should already be in storage
function goblins:spawn(def)
    --print("spawning def: "..dump(def))
    if not def.name then
     print("warning:"," def name not found!")
        return
    end

  local name 						 = def.name
  local dimension        = def.dimension or "overworld"
	local type_of_spawning = def.type_of_spawning or "ground"
	local biomes           = def.biomes or cave_biomes
	local min_light        = tonumber(def.min_light) or 0
	local max_light        = tonumber(def.max_light) or (minetest.LIGHT_MAX + 1)
	local chance           = def.chance or 1000
  local interval         = def.interval or 10
	local aoc              = def.active_object_count or aoc_range --active object count?
	local min_height       = def.min_height or tonumber( mcl_vars.mg_overworld_min ) -- they should be found to the depths of the overworld, MC2 and Mineclonia also differ in this :P
	local max_height       = def.max_height -10
	local day_toggle       = def.day_toggle
	local on_spawn         = def.on_spawn
	local check_position   = def.check_position


	--local gameinfo = minetest.get_game_info()

	if mcl_mobs and mcl_mobs.spawn_setup then
    print("spawn setup detected")
		mcl_mobs.spawn_setup({
			name             = name,
			dimension        = dimension,
			type_of_spawning = type_of_spawning,
			biomes           = biomes,
			min_light        = min_light,
			max_light        = max_light,
			chance           = chance,
			aoc              = aoc,
			min_height       = min_height,
			max_height       = max_height,
			day_toggle       = day_toggle,
			on_spawn         = on_spawn,
		})
	else
		--mc2 or voxelibre
    print("spawn specific used")
		mcl_mobs:spawn_specific(name, dimension, type_of_spawning, biomes, min_light, max_light, interval, chance, aoc, min_height, max_height, day_toggle, on_spawn, check_position)
	end

end

function goblins.node_sound_stone_defaults(...)
    mcl_sounds.node_sound_stone_defaults(...)
end

function goblins.node_sound_dirt_defaults(...)
    mcl_sounds.node_sound_dirt_defaults(...)
end

function goblins.node_sound_leaves_defaults(...)
    mcl_sounds.node_sound_leaves_defaults(...)
end

function goblins.goblin_dog_rightclick(self, clicker)
	if self:feed_tame(clicker, 4, true, true) then return end
  -- if mcl_mobs.mob_class:feed_tame(clicker, 4, true, true) then return end
	-- protect & capture are set to false in mcl_mobs
end
