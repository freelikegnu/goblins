* Update 2024/11/15: Nil check for invis
* Update 2024/09/23: Fix crash in attack function, add rail corridor lairs.
* Update 2024/09/14: Repixture Support added (requires mobs redo)
* Update 2024/09/12: More fixes for HR: mushrooms, torches and fire.
* Update 2024/09/09B: More fixes for HR
* Update 2024/09/09: Hotfix bad init entry
* Update 2024/09/08: Preliminary support for Hades Revisited
* Update 2024/09/07: Added Goblin Lairs in mapgen generated dungeons
* Update 2024/09/04: Fix 5.4 compatability and fix really stupid vector math mistakes
* Update 2024/09/02: Goblin food knowledge limited to their territories.  Goblin chests may have other foods.
* Update 2024/08/31: Goblins now learn what tasty yummies are on the server from observing snacking players. This is designed to work with any food mod in a game.  
* Update 2024/08/25: Cleanup code for database and biome selection. Handle MCLA light levels correctly.  
* Update 2024/08/24: More MCLA fixes for goblin velocity and spawning, gobdog crash on drop fixed
* Update 2024/08/23: use Mineclonia spawning API, node item drop fix 
* Update 2024/08/22: Fix crashing on Mineclonia game startup
* Update 2024/01/15: Fix settings (again) and special gift description check
* Update 2024/01/13:
  *  Fix incorrect dates in changelog :P 
  * Added "pushable" property for mob collisions when using Mobs Redo.
  * Reduced goblin same item acceptance
  * Added enchanted goblin gifts support for Mineclone/Mineclonia
* Update 2024/01/04 fix some annoying text handling
* Update 2024/01/03 added some visual effects to goblin interacts
* Update 2024/01/02 Housekeeping prior to another release and more fixes. 
* Update 2023/12/30 Many more fixes and improvements (goblin attack conditions and gobdog interactions) and fixing crash with Mineclonia! Gonna have to do some housekeeping on this mess.
* Update 2023/12/28 fixes and improvements for comptability and trading
* Update 2023/12/14 reduce danger digging overhead
* Update 2023/12/13 Fix settings and behaviors
* Update 2023/12/11 Fix tool equip crashing bug as suggested by rusty-snake
* Update 2022/12/17 MC2 Nodefixes suggested by Corax, Thanks!
* Update 2022/11/06 MC2 spawning fixes
* Update 2022/11/03 Support added for MineClone 2!! ( https://content.minetest.net/packages/Wuzzy/mineclone2/ )
* Update 2022/10/29 fix settings and other vars, move spawning to mod_storage
* Update 2022/10/25 hotfix  debug crashing
* Update 2022/10/24 Goblin chests added, goblin related items moved to mod_storage!
* Update 2022/09/29 fix redefined var
* Update 2022/08/25 replace old vectors with vector.new()
* Update 2022/08/21 replace deprecated setyaw()
* Update 2021/03/21 Various object checks have been added to eliminate crashes.
* Update 2020/07/12 Goblins dig to safety! Also, random tool selection weight fixes. 
* Update 2020/07/06 added moss node and removed glow from cobblemoss node for better visuals
  more variations for goblin tools and appropriate tools/nodes are chosen for tasks
* Update 2020/06/29 fixed a crash with protection enabled (such as with minecarts). Thanks to rusty-snake for reporting this!
* Update 2020/06/28 fixed a crashing bug in hud code, attached tools to goblins, new goblin animation and texture mods
* Update 2020/06/18 Optional HUD (enable in Minetest options menu)
* Update 2020/06/11 Custom Goblins and Gobdogs can be defined in goblins_custom.lua 
 attacking goblins can now be appeased through trade
* Update 2020/06/07 Goblin and Gobdog spawning is now configured from goblins_spawning.lua (at least until there is a way to easily change these with the setting menu :P )
* Update 2020/06/06 There are many settings now accessible from the minetest menu -> "settings" tab -> "all settings" -> "mods" -> "goblins" list! these can also be defined in the settingtypes.txt
* Update 2020/06/04 Mobs now defend each other (and will defend other mobs with a simple modification to those mobs)
* Update 2020/05/21 Aggro on player wielding a weapon.  Trade affected by player aggression. 
* Update 2020/05/09 Territorial enhancements: 
  The more goblins you trade with from a territory the better and more frequent drops you receive from trading! 
  Multiplayer works with territory and trade relations, each player has their own relations with goblins and territories.
  Internationalization support begun to allow for translations
   Personalized drops from goblins when you have learned their name and territory
* Update 2020/05/05 Goblins have territories defined by chunk! 
   Optional HungerNG and Bonemeal mod support thanks to orbea!
* Update 2020/05/01 Goblins have names!
* Update 2020/04/21: Gobdogs added!
* Update 2020/04/18: Implemented tunneling function from duane-r's goblins. Behavioral functions now get their own file.
* Update 2020/04/17: Added TenPlus1 [url=https://notabug.org/TenPlus1/ambience]ambience[/url]  support as optional dependency. Weaker goblins are more timid around player.
more variety in sounds added.
* Update 2020/04/13: Goblin Fungiler added.  
* Update 2020/04/11: Goblins get some updates and fixes. Should work with Minetest 5.30 (dev)[/spoiler]
